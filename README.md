# VPN con Wireguard en Docker

## Requisitos

1. [Docker Engine](https://docs.docker.com/engine/install/)
2. [Docker Compose](https://docs.docker.com/compose/install/)

## Instalación

1. Clonar el repositorio:`` git clone https://gitlab.com/dd.ven/docker-wireguard.git``
2. Acceder al directorio: ``cd docker-wireguard``
3. Crear una copia de ``.env.sample`` como ``.env``: ``cp .env.sample .env``
4. Modicficar las variables en ``.env`` con un editor de texto: ``nano .env``

````
# Variables

#Zona horaria
TZ=Europe/Madrid

#URL o IP servidor (optional)
SERVERURL=auto 

#Puerto UDP para conexión (optional)
SERVERPORT=51820

#Cantidad de clientes(optional)
PEERS=5

#DNS para los clinetes conectados(optional)
PEERDNS=8.8.8.8

#Red interna(optional)
INTERNAL_SUBNET=10.13.13.0
````

5. Desplegar con docker compose: ``docker-compose up -d``

Para escanear las claves barcode vamos a los logs del contenedor: ``docker logs wireguard``

Para acceder a los QR introducimos el siguiente codigo terminado con el número del peer a visualizar``docker exec -it wireguard /app/show-peer 1``

Para visualizar la configuracion aztual y todas las claves entramos mediante bash al contenedor: ``sudo docker exec -i -t wireguard /bin/bash`` y tecleamos ``wg``